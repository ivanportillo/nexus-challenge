import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const InputBox = styled('input')``;

export const Input = props => <InputBox {...props} />;

Input.propTypes = {
  className: PropTypes.string,
  placeholder: PropTypes.string,
};

Input.defaultProps = {
  className: '',
  placeholder: '',
};

export default styled(Input)`
  border-radius: 0.3rem;
  padding-left: 0.5rem;
  width: 100%;
  height: 3.5rem;
  border: 0.1rem solid lightgrey;

  ${InputBox} {
    outline: none;
    border: 0;
  }
`;
