import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import ContactList from './ContactList';
import Layout from './Layout';
import StatusBar from './StatusBar';

import Contacts from '../services/contacts';

class AddressBook extends Layout {
  static propTypes = {
    ...Layout.propTypes,
    className: PropTypes.string,
  };

  state = { contacts: [], isLoading: false };

  componentDidMount() {
    this.fetchContacts();
  }

  fetchContacts(search) {
    this.setState({ isLoading: true });
    this.request = Contacts.read({ search }).then(contacts => {
      this.request = null;
      this.setState({
        contacts,
        isLoading: false,
      });
    });
  }

  handleChangeSearch = value => {
    this.fetchContacts(value);
  };

  render() {
    const { className } = this.props;
    const { contacts } = this.state;

    const element = super.render();

    if (!element) {
      return null;
    }

    return (
      <main className={className}>
        <StatusBar />
        <ContactList
          items={contacts}
          onChangeSearch={this.handleChangeSearch}
          isLoadingContacts={this.state.isLoading}
        />
        {element}
      </main>
    );
  }
}

export default styled(AddressBook)``;
