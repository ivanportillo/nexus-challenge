import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const FirstName = styled('span')``;
const LastName = styled('span')``;

const capitalize = str => str.charAt(0).toUpperCase() + str.slice(1);

export const Name = ({ firstName, lastName, className }) => (
  <div className={className}>
    <FirstName>{capitalize(firstName)}</FirstName>
    <LastName>{capitalize(lastName)}</LastName>
  </div>
);

Name.propTypes = {
  className: PropTypes.string,
  firstName: PropTypes.string,
  lastName: PropTypes.string,
};

Name.defaultProps = {
  className: '',
  firstName: '',
  lastName: '',
};

export default styled(Name)`
  display: flex;
  flex-direction: row;
  justify-content: center;
  font-size: 2rem;
  font-weight: bold;

  ${LastName} {
    padding-left: 0.5rem;
  }
`;
