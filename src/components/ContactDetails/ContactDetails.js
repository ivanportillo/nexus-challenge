import React, { Component } from 'react';
import { rgba } from 'polished';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Route } from 'react-router-dom';

import Icon from '../Icon';
import Link from '../Link';
import Avatar from '../Avatar';
import Name from './Name';
import ContactMethod from './ContactMethod';

import service from '../../services/contacts';

const Container = styled('section')``;
const Header = styled('header')``;
const PersonalData = styled('div')``;
const ContactMethods = styled('div')``;

class ContactDetails extends Component {
  static defaultProps = {
    className: '',
  };

  static propTypes = {
    className: PropTypes.string,
    ...Route.propTypes,
  };

  state = {
    data: null,
    isLoading: false,
  };

  async componentDidMount() {
    await this.getContactData();
  }

  async componentDidUpdate(prevProps) {
    const {
      match: {
        params: { id },
      },
    } = this.props;
    if (id !== prevProps.match.params.id) {
      await this.getContactData();
    }
  }

  async getContactData() {
    this.setState({ isLoading: true });

    const {
      match: {
        params: { id },
      },
    } = this.props;

    const contact = await service.read({ id });
    this.setState({ data: contact, isLoading: false });
  }

  getContent() {
    const { isLoading, data } = this.state;
    if (data && !isLoading) {
      const { picture, name, phone, cell, email } = data;
      return (
        <>
          <PersonalData>
            <Avatar image={picture.large} />
            <Name firstName={name.first} lastName={name.last} />
          </PersonalData>
          <ContactMethods>
            {phone && <ContactMethod method="phone" info={phone} />}
            {cell && <ContactMethod method="mobile" info={cell} />}
            {email && <ContactMethod method="email" info={email} />}
          </ContactMethods>
        </>
      );
    }

    return 'Loading';
  }

  render() {
    const { className } = this.props;

    return (
      <article className={className}>
        <Header>
          <Link to="/">
            <Icon>arrow_back_ios</Icon>
          </Link>
        </Header>
        <Container>{this.getContent()}</Container>
      </article>
    );
  }
}

export default styled(ContactDetails)`
  background: ${props => props.theme['--color-light']};
  height: calc(100% - 2.5rem);
  position: fixed;
  top: 2.5rem;
  width: 100%;
  //
  ${Header} {
    ${props => props.theme['--font-extra-large']};
    align-items: center;
    display: flex;
    height: 5rem;
    justify-content: center;
    text-align: center;

    ${Icon} {
      height: 5rem;
      left: 0;
      line-height: 5rem;
      position: absolute;
      top: 0;
      width: 5rem;
    }
  }

  ${Container} {
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: row;
    ${PersonalData} {
      display: flex;
      flex-direction: column;
      align-items: center;
      ${Avatar} {
        height: 15rem;
        width: 15rem;
      }

      ${Name} {
        padding-top: 1rem;
      }
    }
    ${ContactMethods} {
      display: flex;
      flex-direction: column;
      padding-left: 4rem;
    }
  }

  @media (${props => props.theme['--screen-medium']}) {
    border-left: 1px solid ${props => rgba(props.theme['--color-dark'], 0.1)};
    left: 32rem;
    width: calc(100% - 32rem);
  }
`;
