import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Icon from '../Icon';

const Title = styled('div')``;
const Method = styled('div')``;
const Info = styled('div')``;
const Image = styled('div')``;

export const getMethodData = method => {
  const methods = {
    phone: {
      icon: 'phone',
      title: 'Phone',
    },
    mobile: {
      icon: 'stay_current_portrait',
      title: 'Mobile',
    },
    email: {
      icon: 'email',
      title: 'Email',
    },
  };

  const defaultMethod = methods.phone;

  return methods[method] || defaultMethod;
};

export const ContactMethod = ({ method, info, className }) => {
  const methodInfo = getMethodData(method);

  return (
    <div className={className}>
      <Image>
        <Icon>{methodInfo.icon}</Icon>
      </Image>
      <Info>
        <Title>{methodInfo.title}</Title>
        <Method>{info}</Method>
      </Info>
    </div>
  );
};

ContactMethod.propTypes = {
  method: PropTypes.string,
  info: PropTypes.string,
  className: PropTypes.string,
};

ContactMethod.defaultProps = {
  className: '',
  method: 'phone',
  info: '',
};

export default styled(ContactMethod)`
  display: flex;
  flex-direction: row;
  ${Info} {
    display: flex;
    flex-direction: column;
    padding-left: 1rem;
  }
  ${Title} {
    color: grey;
    font-size: 1.2rem;
  }
  ${Image} {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }
`;
