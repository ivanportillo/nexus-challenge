import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Contact from './Contact';
import List from '../List';
import Input from '../Input';

const ContactList = ({
  className,
  items,
  onChangeSearch,
  isLoadingContacts,
}) => (
  <div className={className}>
    <Input
      placeholder="Search"
      onChange={event => onChangeSearch(event.target.value)}
    />
    {isLoadingContacts ? 'Loading' : <List items={items} template={Contact} />}
  </div>
);

ContactList.propTypes = {
  className: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.any),
  onChangeSearch: PropTypes.func.isRequired,
  isLoadingContacts: PropTypes.bool,
};

ContactList.defaultProps = {
  className: '',
  items: [],
  isLoadingContacts: false,
};

export default styled(ContactList)`
  margin-top: 1.2rem;
  list-style: none;

  @media (${props => props.theme['--screen-medium']}) {
    width: 32rem;
  }
`;
