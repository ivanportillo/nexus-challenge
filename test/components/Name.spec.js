import React from 'react';
import { render } from 'react-testing-library';

import { Name } from 'Components/ContactDetails/Name';

describe('Component: Name', () => {
  it('renders by default', () => {
    const { container } = render(<Name />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('Should capitalize first name and last name', () => {
    const firstName = 'rick';
    const expectedFirstName = 'Rick';
    const lastName = 'sanchez';
    const expectedLastName = 'Sanchez';
    const { container } = render(
      <Name firstName={firstName} lastName={lastName} />,
    );

    const content = container.firstChild;
    const first = content.childNodes[0];
    const last = content.childNodes[1];

    expect(first.innerHTML).toEqual(expectedFirstName);
    expect(last.innerHTML).toEqual(expectedLastName);
  });
});
