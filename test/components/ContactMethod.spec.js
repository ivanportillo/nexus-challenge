import React from 'react';
import { render } from 'react-testing-library';

import {
  ContactMethod,
  getMethodData,
} from 'Components/ContactDetails/ContactMethod';

describe('Component: ContactMethod', () => {
  it('renders by default', () => {
    const { container } = render(<ContactMethod />);
    expect(container.firstChild).toMatchSnapshot();
  });

  describe('Helper: getMethodData', () => {
    it('should return phone method as default', () => {
      const expectedTitle = 'Phone';
      const expectedIcon = 'phone';

      expect(getMethodData()).toEqual({
        title: expectedTitle,
        icon: expectedIcon,
      });
    });

    it('should return email method given email as method', () => {
      const method = 'email';
      const expectedTitle = 'Email';
      const expectedIcon = 'email';

      expect(getMethodData(method)).toEqual({
        title: expectedTitle,
        icon: expectedIcon,
      });
    });
  });
});
